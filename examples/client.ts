import * as ws from "ws";
import * as $psyrpc from "@psyrpc-js/psyrpc";
import { Rpc } from "@psyrpc-js/proto";
import { Empty, String, File, Boolean } from "./generated/example";
import { Foobar as GeneratedFoobar } from "./generated/example.psyrpc";
import { exit } from "process";

const args: string[] = process.argv.slice(2);

class Simple implements $psyrpc.transfer.Interface {
    constructor(socket: ws.WebSocket) {
        this.socket = socket;
        this.receivedMessages = [];
        this.onMessageReceived = null;

        this.socket.onerror = this.error.bind(this);
        this.socket.onmessage = this.message.bind(this);
    }

    write(message: Rpc): void {
        const encoded = message.serializeBinary();
        this.socket.send(encoded);
    }

    async read(waitFor: $psyrpc.milliseconds.Interface): Promise<Rpc> {
        return new Promise((resolve, reject) => {
            const onMessage = (event: ws.MessageEvent) => {
                clearTimeout();
                this.onMessageReceived = null;

                try {
                    resolve(Rpc.deserializeBinary(event.data as any));
                } catch (e) {
                    reject(e);
                }
            };

            if (this.receivedMessages.length) {
                onMessage(this.receivedMessages.shift() as ws.MessageEvent);
                return;
            }

            this.onMessageReceived = onMessage.bind(this);

            setTimeout(() => {
                reject("timeout while waiting for response");
            }, waitFor.value);
        });
    }

    private error(event: ws.ErrorEvent): void {
        this.onWriteError?.(event.error);
    }

    private message(event: ws.MessageEvent): void {
        this.receivedMessages.push(event);
        if (this.onMessageReceived) {
            const firstEvent = this.receivedMessages.shift();
            this.onMessageReceived(firstEvent as ws.MessageEvent);
        }
    }

    onWriteError?: (error: Event) => void;

    private socket: ws.WebSocket;
    private receivedMessages: ws.MessageEvent[];
    private onMessageReceived: ((event: ws.MessageEvent) => void) | null;
}

class Foobar extends $psyrpc.Service<$psyrpc.Call, typeof $psyrpc.Call> {
    constructor(channel: $psyrpc.channel.Interface) {
        super(channel, $psyrpc.Call);
    }

    async uname(parameter: Empty, writeErrorCallback: $psyrpc.call.OnWriteError,
        waitFor: $psyrpc.milliseconds.Interface): Promise<String> {
        const methodId: $psyrpc.MethodId = 4175629335;
        const customMethodId: $psyrpc.MethodId = 0;
        const stream = await this.call<
            String, typeof String, Empty>(
                this.serviceId, this.customServiceId, methodId, customMethodId,
                String, writeErrorCallback, waitFor);

        stream.writer.write(parameter, $psyrpc.eot);
        const result = await stream.reader.read(waitFor);
        return new Promise((resolve) => { resolve(result); });
    }
    async ls(parameter: Empty, writeErrorCallback: $psyrpc.call.OnWriteError,
        waitFor: $psyrpc.milliseconds.Interface):
        Promise<$psyrpc.streamreader.Interface<String>> {
        const methodId: $psyrpc.MethodId = 1318888756;
        const customMethodId: $psyrpc.MethodId = 0;
        const stream = await this.call<
            String, typeof String, Empty>(
                this.serviceId, this.customServiceId, methodId, customMethodId,
                String, writeErrorCallback, waitFor);

        stream.writer.write(parameter, $psyrpc.eot);
        return new Promise((resolve) => { resolve(stream.reader); });
    }
    async upload(writeErrorCallback: $psyrpc.call.OnWriteError,
        waitFor: $psyrpc.milliseconds.Interface):
        Promise<$psyrpc.service.Streams<Boolean, File>> {
        const methodId: $psyrpc.MethodId = 2585021584;
        const customMethodId: $psyrpc.MethodId = 0;
        const stream = await this.call<
            Boolean, typeof Boolean, File>(
                this.serviceId, this.customServiceId, methodId, customMethodId,
                Boolean, writeErrorCallback, waitFor);

        return new Promise((resolve) => { resolve(stream); });
    }
    async download(parameter: String, writeErrorCallback: $psyrpc.call.OnWriteError,
        waitFor: $psyrpc.milliseconds.Interface):
        Promise<$psyrpc.streamreader.Interface<File>> {
        const methodId: $psyrpc.MethodId = 1392627865;
        const customMethodId: $psyrpc.MethodId = 0;
        const stream = await this.call<
            File, typeof File, String>(
                this.serviceId, this.customServiceId, methodId, customMethodId,
                File, writeErrorCallback, waitFor);

        stream.writer.write(parameter, $psyrpc.eot);
        return new Promise((resolve) => { resolve(stream.reader); });
    }

    private readonly serviceId: $psyrpc.ServiceId = 4138822173;
    private readonly customServiceId: $psyrpc.ServiceId = 0;
}

let server = new ws.WebSocketServer({ port: 13337 });
const address = args[0];
let client = new ws.WebSocket(address);

client.onopen = async () => {
    let transfer = new Simple(client);
    let channel = new $psyrpc.Channel(transfer);
    let foobar = new GeneratedFoobar(channel);
    // let foobar = new Foobar(channel);

    async function uname() {
        return new Promise<void>(async (resolve, reject) => {
            try {
                const uname = await foobar.uname(new Empty(),
                    (event: Event) => { console.log("reject uname"); reject(event); },
                    $psyrpc.Milliseconds(2000000));
                console.log("uname is " + uname.value);
                resolve();
            } catch (e) {
                reject(e);
            }
        });
    }
    async function ls() {
        return new Promise<void>(async (resolve, reject) => {
            try {
                const reader = await foobar.ls(new Empty(),
                    (event: Event) => { console.log("reject ls"); reject(event); }, $psyrpc.Milliseconds(2000));

                while (!reader.eot()) {
                    const filePath = await reader.read($psyrpc.Milliseconds(2000000));
                    console.log(filePath.value);
                }
                resolve();
            } catch (e) {
                reject(e);
            }
        });
    }

    try {
        console.log("await uname");
        await uname();
        console.log("await ls");
        await ls();
    } catch (e) {
        console.error("failed to rpc \"" + e + "\"");
    }
    exit();
};
