# psyrpc-js

Only implements js-client for https://gitlab.com/Psy-Kai/psyrpc.

# Messag generator

Has to be used in conjunction with [protoc-gen-ts](https://github.com/thesayyn/protoc-gen-ts).

⚠ The code has to be generated with [`no_namespace`](https://github.com/thesayyn/protoc-gen-ts#supported-options).
