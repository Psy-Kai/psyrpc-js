const config = {
    collectCoverage: true,
    coverageDirectory: "coverage",
    coverageProvider: "v8",
    coverageReporters: [
        "cobertura",
        "text",
        "html",
    ],
    projects: [
        "packages/*"
    ]
}

export default config;
