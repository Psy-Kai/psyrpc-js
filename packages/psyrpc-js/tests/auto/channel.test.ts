import { mock } from "jest-mock-extended";
import { Channel } from "../../src/channel";
import { Interface as TransferInterface } from "../../src/transfer";
import { Interface as AscendingNumberGeneratorInterface } from "../../src/ascendingnumbergenerator";
import { Milliseconds } from "../../src/milliseconds";
import { Call, Result, Rpc } from "@psyrpc-js/proto";
import { value as magicConst } from "../../src/magic";

describe("Channel", () => {
    let transfer = mock<TransferInterface>();
    let ascendingNumberGenerator = mock<AscendingNumberGeneratorInterface>();
    let channel = new Channel(transfer, ascendingNumberGenerator);
    let observer = {
        writeError: jest.fn()
    };

    const waitFor = Milliseconds(123);
    const flowControl = BigInt("0x123456789");

    describe("given default state", () => {
        beforeEach(() => {
            channel = new Channel(transfer, ascendingNumberGenerator);
        });

        describe("when read", () => {
            const result: Result = function () {
                let result = new Result();
                result.callId = BigInt(42).toString();
                return result;
            }();

            test("and valid rpc returned", async () => {
                const rpc: Rpc = function () {
                    let rpc = new Rpc();
                    rpc.result = result;
                    rpc.flowControl = flowControl.toString();
                    rpc.magicConst = magicConst.toString();
                    return rpc;
                }();

                transfer.read.mockResolvedValueOnce(rpc);
                ascendingNumberGenerator.next.mockReturnValueOnce(flowControl);

                await expect(channel.read(waitFor)).resolves.toEqual(result);

                expect(transfer.read).toHaveBeenCalled();
                expect(ascendingNumberGenerator.next).toHaveBeenCalled();
            });

            const testFunction = async (rpc: Rpc) => {
                transfer.read.mockResolvedValueOnce(rpc);
                ascendingNumberGenerator.next.mockReturnValueOnce(flowControl);

                await expect(channel.read(waitFor)).rejects.toMatch(/invalid rpc/);

                if (rpc.flowControl && rpc.magicConst) {
                    expect(ascendingNumberGenerator.next).toHaveBeenCalled();
                }
            };

            test("but wrong flow control", () => {
                testFunction(function () {
                    let rpc = new Rpc();
                    rpc.result = result;
                    rpc.flowControl = (flowControl + BigInt(1)).toString();
                    rpc.magicConst = magicConst.toString();
                    return rpc;
                }());
            });

            test("but wrong magic const", () => {
                testFunction(function () {
                    let rpc = new Rpc();
                    rpc.result = result;
                    rpc.flowControl = flowControl.toString();
                    rpc.magicConst = (magicConst + BigInt(1)).toString();
                    return rpc;
                }());
            });

            test("but no result", () => {
                testFunction(function () {
                    let rpc = new Rpc();
                    rpc.flowControl = flowControl.toString();
                    rpc.magicConst = magicConst.toString();
                    return rpc;
                }());
            });

            test("but flow control is not Long", () => {
                testFunction(function () {
                    let rpc: Rpc = new Rpc();
                    rpc.magicConst = magicConst.toString();
                    return rpc;
                }());
            });

            test("but magic const is not Long", () => {
                testFunction(function () {
                    let rpc: Rpc = new Rpc();
                    rpc.flowControl = flowControl.toString();
                    return rpc;
                }());
            });

            test("but neither flow control nor magic const is not Long", () => {
                testFunction(function () {
                    let rpc: Rpc = new Rpc();
                    return rpc;
                }());
            });
        });

        describe("when onWriteError", () => {
            test("with error", () => {
                transfer.onWriteError?.(new Event(""));
            });
        });

        describe("when construct", () => {
            test("without AscendingNumberGenerator", () => {
                channel = new Channel(transfer);
            });
        });
    });

    describe("given onWrittenError set", () => {
        beforeEach(() => {
            channel = new Channel(transfer, ascendingNumberGenerator);
            channel.onWriteError = observer.writeError;
        });

        describe("when write", () => {
            const call: Call = function () {
                let call = new Call();
                call.id = BigInt(1337).toString();
                return call;
            }();
            const rpc = function () {
                let rpc = new Rpc();
                rpc.call = call;
                rpc.flowControl = flowControl.toString();
                rpc.magicConst = magicConst.toString();
                return rpc;
            }();

            test("with message", () => {

                ascendingNumberGenerator.next.mockReturnValueOnce(flowControl);

                channel.write(call);

                expect(ascendingNumberGenerator.next).toHaveBeenCalled();
                expect(transfer.write).toHaveBeenCalledWith(rpc);
            });
        });

        describe("when onWriteError", () => {
            test("with error", () => {
                expect(transfer.onWriteError).not.toBe(undefined);

                transfer.onWriteError?.(new Event("Hello Test"));
                expect(observer.writeError).toHaveBeenCalled();
            });
        });
    });
});
