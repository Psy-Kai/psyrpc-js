import { mock } from "jest-mock-extended";
import { BytesValue, Package, StringValue } from "@psyrpc-js/proto";
import { Interface as CallInterface } from "../../../src/call";
import { Stream as Writer } from "../../../src/stream/writer";
import { Interface as AscendingNumberGeneratorInterface }
    from "../../../src/ascendingnumbergenerator";
import { eot as streamEot } from "../../../src/stream/eot";

describe("Reader", () => {
    let call = mock<CallInterface>();
    let ascendingNumberGenerator = mock<AscendingNumberGeneratorInterface>();
    let writer = new Writer(call, ascendingNumberGenerator);
    const eot = true;

    const parameterMsg = function (): StringValue {
        let resultMsg = new StringValue();
        resultMsg.value = "Hello World";
        return resultMsg;
    };
    const packageMsg = function (eot: boolean, parameterMsg?: StringValue): Package {
        let packageMsg = new Package();
        packageMsg.index = BigInt(1).toString();
        if (parameterMsg) {
            packageMsg.data = new BytesValue();
            packageMsg.data.value = parameterMsg.serializeBinary();
        }
        packageMsg.eot = eot;
        return packageMsg;
    };

    describe("given active call", () => {
        beforeEach(() => {
            writer = new Writer(call, ascendingNumberGenerator);
        });

        describe("when write", () => {
            test("without neither eot nor payload", () => {
                expect(() => { writer.write() }).toThrowError(/undefined/);
            });

            test("with eot", () => {
                ascendingNumberGenerator.next.mockReturnValueOnce(BigInt(1));

                expect(writer.write(undefined, streamEot));

                expect(call.write).toHaveBeenCalledWith(packageMsg(eot));
                expect(writer.eot()).toEqual(true);
            });

            test("with parameter", () => {
                ascendingNumberGenerator.next.mockReturnValueOnce(BigInt(1));

                expect(writer.write(parameterMsg()));

                expect(call.write).toHaveBeenCalledWith(packageMsg(!eot, parameterMsg()));
                expect(writer.eot()).toEqual(false);
            });

            test("with parameter and eot", () => {
                ascendingNumberGenerator.next.mockReturnValueOnce(BigInt(1));

                expect(writer.write(parameterMsg(), streamEot));

                expect(call.write).toHaveBeenCalledWith(packageMsg(eot, parameterMsg()));
                expect(writer.eot()).toEqual(true);
            });
        });

        describe("when construct", () => {
            test("without ascending number generator", () => {
                new Writer(call);
            });
        });
    });

    describe("given inactive call", () => {
        beforeEach(() => {
            writer = new Writer(call, ascendingNumberGenerator);
            ascendingNumberGenerator.next.mockReturnValueOnce(BigInt(1));
            writer.write(parameterMsg(), eot);
        });

        describe("when write", () => {
            test("with ", () => {
                expect(() => { writer.write(parameterMsg()) }).toThrow(/invalid call/);
            });
        });
    });
});
