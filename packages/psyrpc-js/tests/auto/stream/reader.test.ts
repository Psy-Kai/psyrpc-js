import { mock } from "jest-mock-extended";
import { BytesValue, Package, StringValue } from "@psyrpc-js/proto";
import { Interface as CallInterface } from "../../../src/call";
import { Stream as Reader } from "../../../src/stream/reader";
import { Milliseconds } from "../../../src/milliseconds";
import { Interface as AscendingNumberGeneratorInterface }
    from "packages/psyrpc-js/src/ascendingnumbergenerator";

describe("Reader", () => {
    let call = mock<CallInterface>();
    let ascendingNumberGenerator = mock<AscendingNumberGeneratorInterface>();
    let resultType = StringValue;
    let reader = new Reader(resultType, call, ascendingNumberGenerator);
    const waitFor = Milliseconds(1337);

    const resultMsg = function (): StringValue {
        let resultMsg = new StringValue();
        resultMsg.value = "Hello World";
        return resultMsg;
    };
    const packageMsg = function (defaultData: BytesValue | null = new BytesValue()): Package {
        let packageMsg = new Package();
        if (defaultData) {
            packageMsg.data = defaultData;
            packageMsg.data.value = resultMsg().serializeBinary();;
        }
        packageMsg.index = BigInt(1).toString();
        return packageMsg;
    };

    describe("given active call", () => {
        beforeEach(() => {
            reader = new Reader(resultType, call, ascendingNumberGenerator);
        });

        describe("when read", () => {
            test("and no eot", async () => {
                call.read.mockResolvedValueOnce(packageMsg());
                ascendingNumberGenerator.next.mockReturnValueOnce(BigInt(1));

                await expect(reader.read(waitFor)).resolves.toEqual(resultMsg());

                expect(call.read).toHaveBeenCalledWith(waitFor);
                expect(reader.eot()).toBe(false);
            });

            test("and eot", async () => {
                const packageMessage = function () {
                    let packageMessage = packageMsg();
                    packageMessage.eot = true;
                    return packageMessage;
                }();
                call.read.mockResolvedValueOnce(packageMessage);
                ascendingNumberGenerator.next.mockReturnValueOnce(BigInt(1));

                await expect(reader.read(waitFor)).resolves.toEqual(resultMsg());

                expect(call.read).toHaveBeenCalledWith(waitFor);
                expect(reader.eot()).toBe(true);
            });

            test("but read fails", async () => {
                const rejectMessage = "rejected";
                call.read.mockRejectedValueOnce(rejectMessage);

                await expect(reader.read(waitFor)).rejects.toEqual(rejectMessage);

                expect(call.read).toHaveBeenCalledWith(waitFor);
            });

            test("but no package id", async () => {
                const packageMessage = function () {
                    let packageMessage = packageMsg();
                    packageMessage.index = "";
                    return packageMessage;
                }();
                call.read.mockResolvedValueOnce(packageMessage);
                ascendingNumberGenerator.next.mockReturnValueOnce(BigInt(1));

                await expect(reader.read(waitFor)).rejects.toMatch(/id/);

                expect(call.read).toHaveBeenCalledWith(waitFor);
            });

            test("but wrong package id", async () => {
                const packageMessage = function () {
                    let packageMessage = packageMsg();
                    packageMessage.index = BigInt(2).toString();
                    return packageMessage;
                }();
                call.read.mockResolvedValueOnce(packageMessage);
                ascendingNumberGenerator.next.mockReturnValueOnce(BigInt(1));

                await expect(reader.read(waitFor)).rejects.toMatch(/id/);

                expect(call.read).toHaveBeenCalledWith(waitFor);
            });

            test("but invalid data", async () => {
                const packageMessage = function () {
                    let packageMessage = packageMsg();
                    packageMessage.data = new BytesValue();
                    packageMessage.data.value = Uint8Array.from([1, 2, 3, 4]);
                    return packageMessage;
                }();
                call.read.mockResolvedValueOnce(packageMessage);
                ascendingNumberGenerator.next.mockReturnValueOnce(BigInt(1));

                await expect(reader.read(waitFor)).rejects.toMatch(/decode/);

                expect(call.read).toHaveBeenCalledWith(waitFor);
            });

            test("and no data", async () => {
                const resultMessage = function () {
                    return new StringValue();
                }();
                const packageMessage = function () {
                    let packageMessage = packageMsg(null);
                    return packageMessage;
                }();
                call.read.mockResolvedValueOnce(packageMessage);
                ascendingNumberGenerator.next.mockReturnValueOnce(BigInt(1));

                await expect(reader.read(waitFor)).resolves.toEqual(resultMessage);

                expect(call.read).toHaveBeenCalledWith(waitFor);
            });

            test("and no data bytes", async () => {
                const resultMessage = function () {
                    return new StringValue();
                }();
                const packageMessage = function () {
                    let packageMessage = packageMsg();
                    packageMessage.data = new BytesValue();
                    return packageMessage;
                }();
                call.read.mockResolvedValueOnce(packageMessage);
                ascendingNumberGenerator.next.mockReturnValueOnce(BigInt(1));

                await expect(reader.read(waitFor)).resolves.toEqual(resultMessage);

                expect(call.read).toHaveBeenCalledWith(waitFor);
            });
        });

        describe("when construct", () => {
            test("with no ascending number generator", () => {
                reader = new Reader(resultType, call);
            });
        });
    });

    describe("given inactive call", () => {
        beforeEach(async () => {
            reader = new Reader(resultType, call, ascendingNumberGenerator);

            const packageMessage = function () {
                let packageMessage = packageMsg();
                packageMessage.eot = true;
                return packageMessage;
            }();
            call.read.mockResolvedValueOnce(packageMessage);
            ascendingNumberGenerator.next.mockReturnValueOnce(BigInt(1));
            await reader.read(waitFor);
        });

        describe("when read", () => {
            test("with ", async () => {
                await expect(reader.read(waitFor)).rejects.toMatch(/no call/);
            });
        });
    });
});
