import { AscendingNumberGenerator } from "../../src/ascendingnumbergenerator";

const unsigned: boolean = true;

describe("AscendingNumberGenerator.next", () => {
    let generator: AscendingNumberGenerator;

    describe("given default state", () => {
        beforeEach(() => {
            generator = new AscendingNumberGenerator();
        });

        describe("when next", () => {
            test("with ", () => {
                expect(generator.next().toString()).toStrictEqual(BigInt(1).toString());
            });
        });
    });

    describe("given at last id", () => {
        beforeEach(() => {
            generator = new AscendingNumberGenerator(BigInt("0xffffffffffffffff"));
        });

        describe("when next", () => {
            test("with ", () => {
                expect(generator.next().toString()).toStrictEqual(BigInt(1).toString());
            });
        });
    });
})
