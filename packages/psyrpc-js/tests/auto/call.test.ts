
import { mock } from "jest-mock-extended";
import { XxHashedSignature, Call as ProtoCall, CallInit, Result, Package, BytesValue } from "@psyrpc-js/proto";
import { Milliseconds } from "../../src/milliseconds";
import { Call } from "../../src/call";
import { Interface as ChannelInterface } from "../../src/channel";

describe("Call", () => {
    const id = BigInt("0x123456789");
    let channel = mock<ChannelInterface>();
    let call = new Call(id, channel);
    let observer = {
        writeError: jest.fn()
    };
    const waitFor = Milliseconds(1337);

    describe("given default state", () => {
        beforeEach(() => {
            call = new Call(id, channel);
        });

        describe("when init", () => {
            const serviceId = 11;
            const customServiceid = 21;
            const methodId = 12;
            const customMethodId = 22;

            const callMsg: ProtoCall = function () {
                let call = new ProtoCall();
                call.id = id.toString();
                call.init = new CallInit();
                call.init.service = new XxHashedSignature();
                call.init.service.value = serviceId;
                call.init.service.customIdToAvoidCollision = customServiceid;
                call.init.method = new XxHashedSignature();
                call.init.method.value = methodId;
                call.init.method.customIdToAvoidCollision = customMethodId;
                return call;
            }();

            test("and valid call message returned", async () => {
                const resultMsg: Result = function () {
                    let result = new Result();
                    result.callId = id.toString();
                    result.init = true;
                    return result;
                }();

                channel.read.mockResolvedValueOnce(resultMsg);

                await expect(call.init(serviceId, customServiceid,
                    methodId, customMethodId, waitFor)).resolves.not.toThrow();

                expect(channel.write).toHaveBeenCalledWith(callMsg);
                expect(channel.read).toHaveBeenCalledWith(waitFor);
            });

            test("but read fails", async () => {
                const rejectMessage: string = "rejected";
                channel.read.mockRejectedValueOnce(rejectMessage);

                await expect(call.init(serviceId, customServiceid,
                    methodId, customMethodId, waitFor)).rejects.toEqual(rejectMessage);

                expect(channel.write).toHaveBeenCalledWith(callMsg);
                expect(channel.read).toHaveBeenCalledWith(waitFor);
            });

            test("but wrong call id", async () => {
                const resultMsg: Result = function () {
                    let result = new Result();
                    result.callId = (id + BigInt(1)).toString();
                    result.init = true;
                    return result;
                }();

                channel.read.mockResolvedValueOnce(resultMsg);

                await expect(call.init(serviceId, customServiceid,
                    methodId, customMethodId, waitFor)).rejects.toMatch(/id/);

                expect(channel.write).toHaveBeenCalledWith(callMsg);
                expect(channel.read).toHaveBeenCalledWith(waitFor);
            });

            test("but init is undefined", async () => {
                const resultMsg: Result = function () {
                    let result = new Result();
                    result.callId = id.toString();
                    return result;
                }();

                channel.read.mockResolvedValueOnce(resultMsg);

                await expect(call.init(serviceId, customServiceid,
                    methodId, customMethodId, waitFor)).rejects.toMatch(/init/);

                expect(channel.write).toHaveBeenCalledWith(callMsg);
                expect(channel.read).toHaveBeenCalledWith(waitFor);
            });

            test("but init is false", async () => {
                const resultMsg: Result = function () {
                    let result = new Result();
                    result.callId = id.toString();
                    result.init = false;
                    return result;
                }();

                channel.read.mockResolvedValueOnce(resultMsg);

                await expect(call.init(serviceId, customServiceid,
                    methodId, customMethodId, waitFor)).rejects.toMatch(/init/);

                expect(channel.write).toHaveBeenCalledWith(callMsg);
                expect(channel.read).toHaveBeenCalledWith(waitFor);
            });
        });
        describe("when read", () => {
            const packageMsg: Package = function () {
                let packageMsg = new Package();
                packageMsg.index = BigInt("0x123456789").toString();
                packageMsg.data = new BytesValue();
                return packageMsg;
            }();
            test("and valid result returned", async () => {
                const resultMsg: Result = function () {
                    let result = new Result();
                    result.callId = id.toString();
                    result.result = packageMsg;
                    return result;
                }();

                channel.read.mockResolvedValueOnce(resultMsg);

                await expect(call.read(waitFor)).resolves.toEqual(packageMsg);

                expect(channel.read).toHaveBeenCalledWith(waitFor);
            });
            test("but read fails", async () => {
                const rejectMessage: string = "rejected";
                channel.read.mockRejectedValueOnce(rejectMessage);

                await expect(call.read(waitFor)).rejects.toEqual(rejectMessage);

                expect(channel.read).toHaveBeenCalledWith(waitFor);
            });
            test("but wrong call id", async () => {
                const resultMsg: Result = function () {
                    let result = new Result();
                    result.callId = (id + BigInt(1)).toString();
                    result.result = packageMsg;
                    return result;
                }();

                channel.read.mockResolvedValueOnce(resultMsg);

                await expect(call.read(waitFor)).rejects.toMatch(/id/);

                expect(channel.read).toHaveBeenCalledWith(waitFor);
            });
            test("but no result", async () => {
                const resultMsg: Result = function () {
                    let result = new Result();
                    result.callId = id.toString();
                    return result;
                }();

                channel.read.mockResolvedValueOnce(resultMsg);

                await expect(call.read(waitFor)).rejects.toMatch(/call/);

                expect(channel.read).toHaveBeenCalledWith(waitFor);
            });
        });

        describe("when onWriteError", () => {
            test("with error", () => {
                channel.onWriteError?.(new Event(""));
            });
        });
    });

    describe("given onWriteError set", () => {
        beforeEach(() => {
            call = new Call(id, channel);
            call.onWriteError = observer.writeError;
        });

        describe("when write", () => {
            const packageMsg: Package = function () {
                let packageMsg = new Package();
                packageMsg.index = BigInt("0x1234567890").toString();
                return packageMsg;
            }();
            const callMsg: ProtoCall = function () {
                let call = new ProtoCall();
                call.id = id.toString();
                call.parameter = packageMsg;
                return call;
            }();

            test("with package", () => {
                call.write(packageMsg);

                expect(channel.write).toHaveBeenCalledWith(callMsg);
            });
        });

        describe("when onWriteError", () => {
            test("with error", () => {
                channel.onWriteError?.(new Event(""));
                expect(observer.writeError).toHaveBeenCalled();
            });
        });
    });
})
