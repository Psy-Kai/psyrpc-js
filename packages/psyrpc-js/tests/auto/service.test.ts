import { mock } from "jest-mock-extended";
import { Service } from "../../src/service";
import { Interface as CallInterface } from "../../src/call";
import { Interface as ChannelInterface } from "../../src/channel";
import { Interface as AscendingNumberGeneratorInterface }
    from "../../src/ascendingnumbergenerator";
import { MethodId, ServiceId } from "../../src/id";
import { Milliseconds as MillisecondsValue, Interface as Milliseconds }
    from "../../src/milliseconds";
import { Int32Value, Package, StringValue } from "@psyrpc-js/proto";
import { OnWriteError } from "../../src/transfer";
import { eot } from "../../src/stream/eot";

describe("Service", () => {
    let channel = mock<ChannelInterface>();
    let ascendingNumberGenerator = mock<AscendingNumberGeneratorInterface>();
    let call = {
        new: jest.fn(),
        init: jest.fn(),
        read: jest.fn(),
        write: jest.fn()
    }
    let writeErrorCallback = jest.fn();
    let callObject: null | Call = null;
    class Call implements CallInterface {
        constructor(id: bigint, channel: ChannelInterface) {
            call.new(id, channel);
            callObject = this;
        }
        async init(serviceId: ServiceId, customServiceid: ServiceId,
            methodId: MethodId, customMethodId: MethodId,
            waitFor: Milliseconds): Promise<void> {
            call.init(serviceId, customServiceid, methodId, customMethodId, waitFor);
        }
        write(_: Package): void {
            call.write();
        }
        async read(_: Milliseconds): Promise<Package> {
            return call.read();
        }
        onWriteError?: OnWriteError;
    }
    let callType = Call;
    let service = new Service(channel, callType, ascendingNumberGenerator);
    const id = {
        service: 1,
        method: 3,
        custom: {
            service: 2,
            method: 4
        }
    }
    const waitFor = MillisecondsValue(1);
    const resultType = Int32Value;

    beforeEach(() => {
        service = new Service(channel, callType, ascendingNumberGenerator);
    });

    describe("given no call in progress", () => {
        describe("when call", () => {
            test("and init succeeds", async () => {
                const callId = BigInt(42);
                ascendingNumberGenerator.next.mockReturnValueOnce(callId);
                call.init.mockResolvedValueOnce(void {});

                await service.call(
                    id.service, id.custom.service, id.method, id.custom.method,
                    resultType, writeErrorCallback, waitFor);

                expect(call.new).toHaveBeenCalledWith(callId, channel);
            });
        });

        describe("when construct", () => {
            test("without ascending number generator", () => {
                new Service(channel, callType);
            });
        });
    });

    describe("given call in progress", () => {
        let stream: any = null;

        beforeEach(async () => {
            const callId = BigInt(42);
            ascendingNumberGenerator.next.mockReturnValueOnce(callId);
            call.init.mockResolvedValueOnce(void {});

            stream = await service.call(
                id.service, id.custom.service, id.method, id.custom.method,
                resultType, writeErrorCallback, waitFor);
        });

        describe("when call", () => {
            test("and no eot", async () => {
                await expect(service.call(
                    id.service, id.custom.service, id.method, id.custom.method,
                    resultType, writeErrorCallback, waitFor)).rejects.toThrow(
                        /Call in Progress/);
            });

            test("and reader eot", async () => {
                call.read.mockResolvedValueOnce(function () {
                    let message = new Package();
                    message.index = BigInt(1).toString();
                    message.eot = true;
                    return message;
                }());
                stream.reader.read(waitFor);

                await expect(service.call(
                    id.service, id.custom.service, id.method, id.custom.method,
                    resultType, writeErrorCallback, waitFor)).rejects.toThrow(
                        /Call in Progress/);
            });

            test("and writer eot", async () => {
                stream.writer.write(null, eot);

                await expect(service.call(
                    id.service, id.custom.service, id.method, id.custom.method,
                    resultType, writeErrorCallback, waitFor)).rejects.toThrow(
                        /Call in Progress/);
            });

            test("and reader eot and writer eot", async () => {
                call.read.mockResolvedValueOnce(function () {
                    let message = new Package();
                    message.index = BigInt(1).toString();
                    message.eot = true;
                    return message;
                }());
                await stream.reader.read(waitFor);
                stream.writer.write(null, eot);
                expect(call.write).toHaveBeenCalled();

                const callId = BigInt(1337);
                ascendingNumberGenerator.next.mockReturnValueOnce(callId);
                call.init.mockResolvedValueOnce(void {});

                await service.call(
                    id.service, id.custom.service, id.method, id.custom.method,
                    resultType, writeErrorCallback, waitFor);

                expect(call.new).toHaveBeenCalledWith(callId, channel);
            });
        });

        test("when writeError", () => {
            const error = new Event("dummy write error");
            callObject?.onWriteError?.(error);
            expect(writeErrorCallback).toHaveBeenCalledWith(error);
        });
    });
})
