import { Call as $Call, CallInit, Package, XxHashedSignature } from "@psyrpc-js/proto";
import { MethodId, ServiceId } from "./id";
import { Interface as Milliseconds } from "./milliseconds";
import { Interface as ChannelInterface } from "./channel";

export type Init = null;
export const init = null;
export type Finished = null;
export const finish = null;

export interface OnWriteError { (event: Event): void; }

export interface Interface {
    init(serviceId: ServiceId, customServiceid: ServiceId,
        methodId: MethodId, customMethodId: MethodId,
        waitFor: Milliseconds): Promise<void>;
    write(payload: Package): void;
    read(waitFor: Milliseconds): Promise<Package>;
    onWriteError?: OnWriteError;
}

export class Call implements Interface {
    constructor(id: bigint, channel: ChannelInterface) {
        this.id = id;
        this.channel = channel;

        this.channel.onWriteError = this.error.bind(this);
    }

    async init(serviceId: ServiceId, customServiceid: ServiceId,
        methodId: MethodId, customMethodId: MethodId,
        waitFor: Milliseconds): Promise<void> {
        let call = new $Call();
        call.id = this.id.toString();
        call.init = new CallInit();
        call.init.service = new XxHashedSignature();
        call.init.service.value = serviceId;
        call.init.service.customIdToAvoidCollision = customServiceid;
        call.init.method = new XxHashedSignature();
        call.init.method.value = methodId;
        call.init.method.customIdToAvoidCollision = customMethodId;

        this.channel.write(call);

        const result = await this.channel.read(waitFor);
        return new Promise((resolve, reject) => {
            if (result.callId === null || result.callId === undefined ||
                (this.id.toString() != result.callId)) {
                reject("invalid call id");
                return;
            }
            if (!result.init) {
                reject("unknown init response");
                return;
            }
            resolve();
        });
    }
    write(message: Package): void {
        let call = new $Call();
        call.id = this.id.toString();
        call.parameter = message;

        this.channel.write(call);
    }
    async read(waitFor: Milliseconds): Promise<Package> {
        const result = await this.channel.read(waitFor);
        return new Promise((resolve, reject) => {
            if (this.id.toString() != result.callId) {
                reject("invalid call id");
                return;
            };
            if (!result.result) {
                reject("unknown call response");
                return;
            }
            resolve(result.result);
        });
    }

    private error(event: Event): void {
        this.onWriteError?.(event);
    }

    onWriteError?: OnWriteError;
    private readonly id: bigint;
    private channel: ChannelInterface;
}
