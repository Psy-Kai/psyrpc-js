import { Package } from "@psyrpc-js/proto";
import { Interface as CallInterface } from "../call";
import { Interface as Milliseconds } from "../milliseconds";
import { Interface as AscendingNumberGeneratorInterface, AscendingNumberGenerator }
    from "../ascendingnumbergenerator";

export interface Deserializable<TResult> {
    new(): TResult;
    deserializeBinary(payload: Uint8Array): TResult;
}

export interface Interface<TResult> {
    read(waitFor: Milliseconds): Promise<TResult>;
    eot(): boolean;
}

export class Stream<TResult, TResultType extends Deserializable<TResult>> implements Interface<TResult> {
    constructor(messageType: TResultType, call: CallInterface,
        ascendingNumberGenerator?: AscendingNumberGeneratorInterface) {
        this.messageType = messageType;
        this.call = call;
        this.ascendingNumberGenerator =
            ascendingNumberGenerator ? ascendingNumberGenerator : new AscendingNumberGenerator();
    }

    async read(waitFor: Milliseconds): Promise<TResult> {
        return new Promise((resolve, reject) => {
            if (this.eot() || (this.call === null)) {
                reject("no call in progress");
                return;
            }

            this.call.read(waitFor).then((packageMessage: Package) => {
                if (packageMessage.index != this.ascendingNumberGenerator.next().toString()) {
                    reject("invalid package id");
                    return;
                }

                try {
                    const result: TResult = this.deserialize(packageMessage);
                    if (packageMessage.eot) {
                        this.call = null;
                    }
                    resolve(result);
                } catch (e) {
                    reject('failed to decode message ("' + e + '")');
                }
            }).catch((reason: any) => {
                this.call = null;
                reject(reason);
            });
        });
    }

    eot(): boolean {
        return this.call === null;
    }

    private deserialize(packageMessage: Package): TResult {
        if (packageMessage.has_data && packageMessage.data.value) {
            return this.messageType.deserializeBinary(packageMessage.data.value);
        }
        return new this.messageType();
    }

    private messageType: TResultType;
    private call: CallInterface | null;
    private ascendingNumberGenerator: AscendingNumberGeneratorInterface;
}
