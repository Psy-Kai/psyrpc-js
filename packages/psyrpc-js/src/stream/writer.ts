import { Package, BytesValue } from "@psyrpc-js/proto";
import { Interface as CallInterface } from "../call";
import { Interface as AscendingNumberGeneratorInterface, AscendingNumberGenerator }
    from "../ascendingnumbergenerator";
import { Eot } from "./eot";

export interface Serializable {
    serializeBinary(): Uint8Array;
}

export interface Interface<T> {
    write(payload?: T, eot?: Eot): void;
    eot(): boolean;
}

export class Stream<TParameter extends Serializable>
    implements Interface<TParameter> {
    constructor(call: CallInterface, ascendingNumberGenerator?: AscendingNumberGeneratorInterface) {
        this.call = call;
        this.ascendingNumberGenerator =
            ascendingNumberGenerator ? ascendingNumberGenerator : new AscendingNumberGenerator();
    }

    write(payload?: TParameter, eot?: Eot): void {
        if (!payload && !eot) {
            throw Error("payload and eot are both undefined");
        }

        if (!this.call) {
            throw Error("invalid call");
        }

        let packageMessage = new Package();
        packageMessage.index = this.ascendingNumberGenerator.next().toString();
        packageMessage.eot = !!eot;
        if (payload) {
            packageMessage.data = new BytesValue();
            packageMessage.data.value = payload.serializeBinary();
        }

        this.call.write(packageMessage);
        if (eot) {
            this.call = null;
        }
    }

    eot(): boolean {
        return this.call === null;
    }

    private call: CallInterface | null;
    private ascendingNumberGenerator: AscendingNumberGeneratorInterface;
}
