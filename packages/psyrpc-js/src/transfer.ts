import { Rpc } from "@psyrpc-js/proto";
import { Interface as Milliseconds } from "./milliseconds";

export interface OnWriteError { (error: Event): void; }

export interface Interface {
    write(message: Rpc): void;
    read(waitFor: Milliseconds): Promise<Rpc>;
    onWriteError?: OnWriteError;
}

export class Simple implements Interface {
    constructor(socket: WebSocket) {
        this.socket = socket;
        this.receivedMessages = [];
        this.onMessageReceived = null;

        this.socket.onerror = this.error.bind(this);
        this.socket.onmessage = this.message.bind(this);
    }

    write(message: Rpc): void {
        const encoded = message.serializeBinary();
        this.socket.send(encoded);
    }
    async read(waitFor: Milliseconds): Promise<Rpc> {
        return new Promise((resolve, reject) => {
            const onMessage = (event: MessageEvent) => {
                clearTimeout();
                this.onMessageReceived = null;

                try {
                    resolve(Rpc.deserializeBinary(event.data));
                } catch (e) {
                    reject(e);
                }
            };

            if (this.receivedMessages.length) {
                onMessage(this.receivedMessages.shift() as MessageEvent);
                return;
            }

            this.onMessageReceived = onMessage.bind(this);

            setTimeout(() => {
                reject("timeout while waiting for response");
            }, waitFor.value);
        });
    }

    private error(event: Event): void {
        this.onWriteError?.(event);
    }

    private message(event: MessageEvent): void {
        this.receivedMessages.push(event);
        if (this.onMessageReceived) {
            const firstEvent = this.receivedMessages.shift();
            this.onMessageReceived(firstEvent as MessageEvent);
        }
    }

    onWriteError?: OnWriteError;

    private socket: WebSocket;
    private receivedMessages: MessageEvent[];
    private onMessageReceived: ((event: MessageEvent) => void) | null;
}
