export interface Interface {
    readonly value: number
}

export function Milliseconds(value: number): Interface {
    return { value: value };
}
