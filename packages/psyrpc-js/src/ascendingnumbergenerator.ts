export interface Interface {
    next(): bigint;
}

const max = BigInt("0xffffffffffffffff");

export class AscendingNumberGenerator implements Interface {
    constructor(value?: bigint) {
        if (value) {
            this.value = value;
        }
    }

    next(): bigint {
        if (this.value < max) {
            this.value += BigInt(1);
        } else {
            this.value = BigInt(1);
        }

        return this.value;
    }

    private value: bigint = BigInt(0);
}
