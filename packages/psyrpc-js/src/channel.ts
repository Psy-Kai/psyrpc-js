import { Call as $Call, Result, Rpc } from "@psyrpc-js/proto";
import { Interface as Milliseconds } from "./milliseconds";
import { Interface as TransferInterface } from "./transfer";
import { value as magicConst } from "./magic";
import { AscendingNumberGenerator, Interface as AscendingNumberGeneratorInterface }
    from "./ascendingnumbergenerator";

export interface OnWriteError { (event: Event): void; }

export interface Interface {
    write(call: $Call): void;
    read(waitFor: Milliseconds): Promise<Result>;
    onWriteError?: OnWriteError;
}

export class Channel implements Interface {
    constructor(transfer: TransferInterface,
        ascendingNumberGenerator?: AscendingNumberGeneratorInterface) {
        this.transfer = transfer;
        this.ascendingNumberGenerator =
            ascendingNumberGenerator ? ascendingNumberGenerator : new AscendingNumberGenerator();

        this.transfer.onWriteError = this.error.bind(this);
    }

    write(call: $Call): void {
        let rpc = this.createRpcToWrite();
        rpc.call = call;
        this.transfer.write(rpc);
    }
    async read(waitFor: Milliseconds): Promise<Result> {
        const message = await this.transfer.read(waitFor);
        return new Promise((resolve, reject) => {
            if (!this.validateRpc(message) || !message.has_result) {
                reject("received invalid rpc response");
            } else {
                resolve(message.result);
            }
        });
    }

    private error(event: Event): void {
        this.onWriteError?.(event);
    }

    private createRpcToWrite(): Rpc {
        let rpc = new Rpc();
        rpc.magicConst = magicConst.toString();
        rpc.flowControl = this.ascendingNumberGenerator.next().toString();
        return rpc;
    }
    private validateRpc(message: Rpc): boolean {
        const flowControlValue = this.ascendingNumberGenerator.next();
        return (magicConst.toString() == message.magicConst) &&
            (flowControlValue.toString() == message.flowControl);
    }

    onWriteError?: OnWriteError;
    private transfer: TransferInterface;
    private ascendingNumberGenerator: AscendingNumberGeneratorInterface;
}
