import { MethodId, ServiceId } from "./id";
import { Interface as CallInterface, OnWriteError } from "./call";
import { Interface as ChannelInterface } from "./channel";
import { Interface as Milliseconds } from "./milliseconds";
import { Interface as AscendingNumberGeneratorInterface, AscendingNumberGenerator }
    from "./ascendingnumbergenerator";
import {
    Interface as StreamWriterInterface,
    Stream as StreamWriter,
    Serializable
} from "./stream/writer";
import {
    Interface as StreamReaderInterface,
    Stream as StreamReader,
    Deserializable
} from "./stream/reader";

export class Streams<TResult, TParameter> {
    constructor(reader: StreamReaderInterface<TResult>, writer: StreamWriterInterface<TParameter>) {
        this.reader = reader;
        this.writer = writer;
    }

    reader: StreamReaderInterface<TResult>;
    writer: StreamWriterInterface<TParameter>;
}

export interface Interface {
    call<TResult, TResultType extends Deserializable<TResult>,
        TParameter extends Serializable>(
            serviceId: ServiceId, customServiceid: ServiceId,
            methodId: MethodId, customMethodId: MethodId,
            resultMessageType: TResultType,
            writeErrorCallback: OnWriteError,
            waitFor: Milliseconds): Promise<Streams<TResult, TParameter>>;
}

class CallData<TResult, TParameter> {
    constructor(call: CallInterface,
        writeErrorCallback: OnWriteError,
        streamReader: StreamReaderInterface<TResult>,
        streamWriter: StreamWriterInterface<TParameter>) {
        this.call = call;
        this.writeErrorCallback = writeErrorCallback;
        this.streamReader = streamReader;
        this.streamWriter = streamWriter;
    }

    call: CallInterface;
    writeErrorCallback: OnWriteError;
    streamReader: StreamReaderInterface<TResult>;
    streamWriter: StreamWriterInterface<TParameter>;
}

interface ConstructableCall<TCall> {
    new(id: bigint, channel: ChannelInterface): TCall;
}

export class Service<TCall extends CallInterface, TCallType extends ConstructableCall<TCall>>
    implements Interface {
    constructor(channel: ChannelInterface,
        callType: TCallType,
        ascendingNumberGenerator?: AscendingNumberGeneratorInterface) {
        this.channel = channel;
        this.callType = callType;
        this.ascendingNumberGenerator = ascendingNumberGenerator ? ascendingNumberGenerator :
            new AscendingNumberGenerator();
    }
    async call<TResult, TResultType extends Deserializable<TResult>,
        TParameter extends Serializable>(
            serviceId: ServiceId, customServiceid: ServiceId,
            methodId: MethodId, customMethodId: MethodId,
            resultMessageType: TResultType,
            writeErrorCallback: OnWriteError,
            waitFor: Milliseconds): Promise<Streams<TResult, TParameter>> {
        if (this.callInProgress()) {
            throw Error("Call in Progress");
        }

        let call = new this.callType(this.ascendingNumberGenerator.next(), this.channel);
        let streamWriter = new StreamWriter<TParameter>(call);
        let streamReader = new StreamReader<TResult, TResultType>(resultMessageType, call);
        this.callData = new CallData(call, writeErrorCallback, streamReader, streamWriter);

        call.onWriteError = this.error.bind(this);
        await call.init(serviceId, customServiceid, methodId, customMethodId, waitFor);
        return new Promise((resolve) => {
            resolve(new Streams<TResult, TParameter>(streamReader, streamWriter));
        });
    }

    private callInProgress(): boolean {
        return this.callData &&
            !(this.callData.streamReader.eot() && this.callData.streamWriter.eot());
    }

    private error(event: Event) {
        let callData = this.callData;
        this.callData = null;
        callData.writeErrorCallback(event);
    }

    private channel: ChannelInterface;
    private callType: TCallType;
    private ascendingNumberGenerator: AscendingNumberGeneratorInterface;
    private callData: any;
}
