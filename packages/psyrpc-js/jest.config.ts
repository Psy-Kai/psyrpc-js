import baseConfig from "../../jest.config.base";

const config = {
    ...baseConfig,
};

export default config;
