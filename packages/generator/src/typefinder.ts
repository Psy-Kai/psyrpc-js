import { DescriptorProto, FileDescriptorProto } from "./google/protobuf/descriptor";

export class TypeFinder {
    constructor(files: FileDescriptorProto[]) {
        this.files = files;
    }

    find(fullQualifiedProtoType: string): FileDescriptorProto | undefined {
        return this.files.find(file => {
            const filePackage = file.package ? '.' + file.package : '';
            return findMessage(file.message_type, filePackage, fullQualifiedProtoType);
        });
    }

    private readonly files: FileDescriptorProto[];
}

function findMessage(
    messages: DescriptorProto[], messageNamespace: string, fullQualifiedProtoType: string):
    boolean {
    return !!messages.find(messageType => {
        const fullQualifiedProtoTypeOnMessage = messageNamespace + '.' + messageType.name;
        if (findMessage(messageType.nested_type,
            fullQualifiedProtoTypeOnMessage,
            fullQualifiedProtoType)) {
            return true;
        }

        return fullQualifiedProtoTypeOnMessage === fullQualifiedProtoType
    });
}
