import { h32 } from 'xxhashjs';
import { FileDescriptorProto, ServiceDescriptorProto } from './google/protobuf/descriptor';
import indentation from './indentation';
import * as method from './method';
import { TypeFinder } from './typefinder';

const sInd = indentation;

export interface ServiceInfo {
    content: string,
    requiredImports: Set<FileDescriptorProto>;
}

export function generate(service: ServiceDescriptorProto, packageName: string,
    typeFinder: TypeFinder): ServiceInfo {
    let requiredImports = new Set<FileDescriptorProto>();

    let content: string = '';
    content += 'export class ' + service.name +
        ' extends $psyrpc.Service<$psyrpc.Call, typeof $psyrpc.Call> {\n';
    content += sInd + 'constructor(channel: $psyrpc.channel.Interface) { super(channel, $psyrpc.Call); }\n\n';
    service.method.forEach(m => {
        const methodInfo = method.generate(m, typeFinder);
        content += methodInfo.content;
        requiredImports.add(methodInfo.input);
        requiredImports.add(methodInfo.output);
        content += '\n';
    });
    content += sInd + 'private readonly serviceId: $psyrpc.ServiceId = ' +
        h32(packageName + '.' + service.name, 0).toString() + '\n';
    content += sInd + 'private readonly customServiceId: $psyrpc.ServiceId = ' + 0 + '\n';
    content += '}\n';

    return {
        content,
        requiredImports
    };
}
