import fs from "fs";
import process from "process";
import * as plugin from "./google/protobuf/compiler/plugin";
import * as file from "./file";
import path from "path";
import { TypeFinder } from "./typefinder";

const suffix = ".psyrpc.ts";

const request = plugin.CodeGeneratorRequest.deserializeBinary(
    new Uint8Array(fs.readFileSync(process.stdin.fd))
);

let files: plugin.CodeGeneratorResponseFile[] = [];

let typeFinder = new TypeFinder(request.proto_file);
request.proto_file.forEach(f => {
    const name = path.join(path.dirname(f.name), path.basename(f.name, ".proto") + suffix);
    files.push(
        new plugin.CodeGeneratorResponseFile({
            name: name,
            content: file.generate(f, typeFinder)
        }));
});

process.stdout.write(new plugin.CodeGeneratorResponse({
    file: files
}).serializeBinary());
