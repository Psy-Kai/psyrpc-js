import { h32 } from "xxhashjs";
import { FileDescriptorProto, MethodDescriptorProto } from "./google/protobuf/descriptor";
import importPathToAliasName from "./importpathtoaliasname";
import indentation from "./indentation";
import { TypeFinder } from "./typefinder";

const mInd = indentation;
const bInd = indentation + indentation;

export interface MethodInfo {
    content: string;
    input: FileDescriptorProto;
    output: FileDescriptorProto;
}

function typeWithoutPackage(fullQualifiedProtoType: string, filePackage: string): string {
    const fullFilePackage = '.' + filePackage;
    return fullQualifiedProtoType.substring(fullFilePackage.length).replace(/\./g, '');
}

interface FullQualifiedType {
    name: string;
    file: FileDescriptorProto;
}

function fullQualifiedTsType(fullQualifiedProtoType: string, typeFinder: TypeFinder): FullQualifiedType {
    const file = typeFinder.find(fullQualifiedProtoType);
    if (!file) {
        throw "No file for " + fullQualifiedProtoType + " found";
    }
    const typeName = typeWithoutPackage(fullQualifiedProtoType, file.package);
    const fullQualifiedTsType = importPathToAliasName(file) + '.' + typeName;

    return {
        name: fullQualifiedTsType,
        file
    };
}

export function generate(method: MethodDescriptorProto, typeFinder: TypeFinder): MethodInfo {
    let content = '';
    const returnType = fullQualifiedTsType(method.output_type, typeFinder);
    const parameterType = fullQualifiedTsType(method.input_type, typeFinder);

    content += mInd + "async " + method.name + "(";

    if (!method.client_streaming) {
        content += "parameter: " + parameterType.name + ", ";
    }
    content += "writeErrorCallback: $psyrpc.call.OnWriteError, ";
    content += "waitFor: $psyrpc.milliseconds.Interface)";

    if (method.client_streaming) {
        content += ": Promise<$psyrpc.service.Streams<" +
            returnType.name + ", " +
            parameterType.name + ">>";
    } else {
        if (method.server_streaming) {
            content += ": Promise<$psyrpc.streamreader.Interface<" +
                returnType.name + ">>";
        } else {
            content += ": Promise<" + returnType.name + ">";
        }
    }
    content += " {\n";
    content += bInd + "const methodId: $psyrpc.MethodId = " + h32(method.name, 0).toString() + "\n";
    content += bInd + "const customMethodId: $psyrpc.MethodId = " + 0 + "\n\n";
    content += bInd + "const stream = await this.call<" + returnType.name + ", typeof " + returnType.name +
        ", " + parameterType.name + ">"
    content += "(this.serviceId, this.customServiceId, methodId, customMethodId, " +
        returnType.name + ", writeErrorCallback, waitFor);\n";

    if (method.client_streaming) {
        content += "\n" + bInd + "return new Promise((resolve) => { resolve(stream); });\n"
    } else {
        content += bInd + "stream.writer.write(parameter, $psyrpc.eot);\n\n";
        if (method.server_streaming) {
            content += bInd + "return new Promise((resolve) => { resolve(stream.reader); });\n"
        } else {
            content += bInd + "const result = await stream.reader.read(waitFor);\n";
            content += bInd + "return new Promise((resolve) => { resolve(result); });\n"
        }
    }

    content += mInd + "}\n";

    const methodInfo: MethodInfo = {
        content,
        input: parameterType.file,
        output: returnType.file,
    };

    return methodInfo;
}
