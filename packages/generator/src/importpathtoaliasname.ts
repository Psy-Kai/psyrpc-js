import path from "path";
import { proto } from "./extensions";
import { FileDescriptorProto } from "./google/protobuf/descriptor";

export default function importPathToAliasName(file: FileDescriptorProto): string {
    const dirname = path.dirname(file.name);
    const basename = path.basename(file.name, proto);
    const filePath = path.posix.join(dirname, basename);
    return '$' + filePath.replace(/\//g, '_');
}
