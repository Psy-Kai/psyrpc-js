import path from "path";
import { proto } from "./extensions";
import { FileDescriptorProto } from "./google/protobuf/descriptor";
import importPathToAliasName from "./importpathtoaliasname";
import * as service from "./service";
import { TypeFinder } from "./typefinder";

export function generate(file: FileDescriptorProto, typeFinder: TypeFinder): string {
    let imports = '';
    imports += 'import * as $psyrpc from "@psyrpc-js/psyrpc";\n';

    let requestedImports = new Set<FileDescriptorProto>();

    let content = '';
    file.service.forEach(s => {
        const serviceInfo = service.generate(s, file.package, typeFinder);
        requestedImports = new Set([...requestedImports, ...serviceInfo.requiredImports]);
        content += serviceInfo.content;
    });

    requestedImports.forEach(requestedImport => {
        const importAlias = importPathToAliasName(requestedImport);
        const filePath = () => {
            if (file.name === requestedImport.name) {
                return path.basename(requestedImport.name, proto);
            } else {
                const fileDir = path.dirname(file.name);
                const relativePath = path.posix.relative(fileDir, requestedImport.name);
                const dirname = path.dirname(relativePath);
                const basename = path.basename(requestedImport.name, proto);
                const pathToModule = path.posix.join(dirname, basename);

                return pathToModule;
            }
        };

        imports += 'import * as ' + importAlias + ' from "./' + filePath() + '";\n';
    });

    return imports + '\n' + content;
}
